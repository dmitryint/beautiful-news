FROM python:3-alpine

ADD ./ /app
WORKDIR /app
RUN apk add --no-cache --virtual .build-deps gcc libxml2-dev libc-dev libxslt-dev linux-headers \
    && apk -U add libxml2 libxslt \
    && pip3 install -r /app/requirements.txt \
    && apk del .build-deps

EXPOSE 8080
CMD [ "uwsgi", "--http", ":8080", "--manage-script-name", "--mount", "/app=run:app" ]
