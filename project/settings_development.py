# -*- coding: utf-8 -*-
DEBUG = True
TESTING = False
REDIS_URL = "redis://localhost:6379"
FEED_TTL = 10
USER_AGENT = ('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36')
