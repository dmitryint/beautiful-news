# -*- coding: utf-8 -*-
DEBUG = False
TESTING = False
REDIS_URL = "unix:///var/redis_socket/redis.sock"
FEED_TTL = 3600
USER_AGENT = ('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36')

