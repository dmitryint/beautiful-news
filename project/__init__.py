from flask import Flask
from flask_redis import FlaskRedis
import logging
import os

app = Flask(__name__)
app.config.from_pyfile(os.environ.get('FLASK_SETTINGS','settings.py'))

handler = logging.getLogger()
handler.setLevel(logging.INFO)
app.logger.addHandler(handler)

redis_store = FlaskRedis(app, charset="utf-8", decode_responses=True)

app.logger.info('REDIS_URL: %s' % app.config['REDIS_URL'])

import project.views
