from project import app, redis_store
from flask import request, Response, send_from_directory
from itertools import tee, filterfalse, chain
from concurrent import futures
from operator import truth
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from readability import Document
import hashlib
import requests


def partition(pred, iterable):
    'Use a predicate to partition entries into false entries and true entries'
    # partition(is_odd, range(10)) --> 0 2 4 6 8   and  1 3 5 7 9
    t1, t2 = tee(iterable)
    return filterfalse(pred, t1), filter(pred, t2)


def get_html(url):
    q = Request(url)
    q.add_header(*app.config['USER_AGENT'])
    return urlopen(q).read()


def beautifully_html(html):
    return Document(html).summary(html_partial=True)


def get_item_hash(item):
    return hashlib.sha256(item).hexdigest()


def fetch_one(url):
    r = requests.get(url)
    r.raise_for_status()
    return r.content


def get_uncached_items(items):
    pipe = redis_store.pipeline()
    with futures.ThreadPoolExecutor(max_workers=10) as executor:
        future_to_url = {executor.submit(fetch_one, item.link.text): item for (item, _) in items}
        for future in futures.as_completed(future_to_url):
            item = future_to_url[future]
            if future.exception() is None:
                data = beautifully_html(future.result())
                feed_hash = get_item_hash(item.link.text.encode())
                pipe.set(feed_hash, data).expire(feed_hash, app.config['FEED_TTL'])
                yield item, data
            else:
                # print('%r generated an exception: %s' % (
                # url, future.exception()))
                yield item, None

        pipe.execute()


def soup_replace_description(item_data):
    item, data = item_data
    item.description.string = data
    return item, data


@app.route('/')
def index():
    return 'Hello World!'


@app.route('/rss/')
def get_rss():
    if request.method == 'GET':
        urls = request.args.getlist('url')
        if any(urls):
            for u in urls:
                pipe = redis_store.pipeline()
                feed_hash = hashlib.sha256(u.encode()).hexdigest()
                cached_xml = redis_store.get(feed_hash)
                soup = BeautifulSoup(cached_xml or get_html(u), 'lxml-xml')
                if not cached_xml:
                    pipe.set(feed_hash, str(soup)).expire(feed_hash, app.config['FEED_TTL']).execute()

                list(map(
                    soup_replace_description,
                    chain.from_iterable([f(x) for f, x in zip(
                        (get_uncached_items, lambda l: l), partition(
                            lambda item_data: truth(item_data[1]),
                            zip(
                                [pipe.get(get_item_hash(i.link.text.encode())) and i for i in soup('item')],
                                pipe.execute()
                            )
                        )
                    )])
                ))

                return Response(str(soup), mimetype='text/xml')
    else:
        return 'Hello World!'


@app.route('/robots.txt')
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])
