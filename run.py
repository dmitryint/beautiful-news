from project import app

if __name__ == "__main__":
    app.config.from_pyfile('settings_development.py')
    app.run()
